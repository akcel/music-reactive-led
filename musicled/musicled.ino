

//physical conditions:
#define LEDPIN 4 //datapin of the leds, power leds with 5v
#define NUMPIXELS 4 //amount of leds
#define MICPIN 32 //datapin of the mic, power mic with 3v3

//constant adjustment parameters
//audio
#define NOISECUTOFF 65 //below this is considered silent
#define DAMPENSTEP 1 //how fast maxVolume reduces
//visual
#define BRIGHTENSTEP 1 //how fast brightness rises
#define BRIGHTNESSTIME 100 //for how long brightness is rised
#define DIMSTEP 2 //how fast brightness reduces
#define HUESTEP 5 //how fast hue advances
#define HUETIME 50 //for how long hue is advanced

//maybe?
//int majorLed = 0; //the led which is the brightest
//later there should be added variation to hue and brightness between the leds
//automatic noise level calibration at startup
//automatic max volume calibration to scale brightenstep

/*      //TODO//
-"tempo": record lastPeakTime, if very long ago, smaller dimstep, and the other way

-brightness rises according to maxvolume rise, not absolute value

*/

int brightnessTimer = 0; //for how long brightness is rised
int hueTimer = 0; //for how long hue is advanced

float brightness = 0; //how much brightness rises
int hue = 0; //how much hue advances

float maxVolume = 0; //volume peaks
float lastMaxVolume = 0; //previous value


    //neopixel setup//
#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
  #include <avr/power.h>
#endif

Adafruit_NeoPixel pixels(NUMPIXELS, LEDPIN, NEO_RGB + NEO_KHZ800);

void setup() {
    #if defined(__AVR_ATtiny85__) && (F_CPU == 16000000)
        clock_prescale_set(clock_div_1);
    #endif
    
    Serial.begin(115200);
    pixels.begin();
    pinMode(LEDPIN, OUTPUT);
    pinMode(MICPIN, INPUT);
}

void loop() {
    float volume = abs(analogRead(MICPIN) - 1880); //read the mic value to volume
    volume = constrain(volume - NOISECUTOFF, 0, 3760); //cuts of noise

        //volume comparison section//
    if (volume > maxVolume){
        //brightness starts to rise up
        maxVolume = volume;
        brightnessTimer = BRIGHTNESSTIME;
        hueTimer = HUETIME;    
    }
    else {
        //reducing maxvolume
        maxVolume = constrain(maxVolume - DAMPENSTEP * maxVolume / 100, 0, 3760);
        
    }

        //brightness&hue section//
    if (brightnessTimer) {
        //rising brightness
        brightness = constrain(brightness + BRIGHTENSTEP * (maxVolume - lastMaxVolume) / 100, 0, 255);
        brightnessTimer--;
        
    }
    else {
        //lowering brightness
        //TODO two different speeds according to "tempo"
        brightness = constrain(brightness - DIMSTEP * brightness / 255, 0, 255);
        lastMaxVolume = maxVolume;
    }
    
    if (hueTimer) {
        //advancing hue
        hue = hue + HUESTEP;
        hueTimer--;
    }
    

        //led section//
    pixels.clear();
    //light all the pixels the same
    int color = pixels.gamma32(pixels.ColorHSV(hue,255 ,brightness));
    for (int i = 0; i < NUMPIXELS; i++){
        pixels.setPixelColor(i, color);
    }
    
    pixels.show();
    
    delayMicroseconds(200);
    /*
    Serial.print(brightness);
    Serial.print(",");
    Serial.print(volume);
    Serial.print(",");
    Serial.print(maxVolume);
    Serial.print(",");
    Serial.println(lastMaxVolume);
    */

}
